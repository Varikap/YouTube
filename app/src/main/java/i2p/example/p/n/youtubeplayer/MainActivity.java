package i2p.example.p.n.youtubeplayer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = (Button) findViewById(R.id.btnPlaySingle);
        Button standAlone = (Button) findViewById(R.id.btnStandAlone);

        btn.setOnClickListener(this);
        standAlone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = null;

        switch (v.getId()){
            case R.id.btnPlaySingle:
                i = new Intent(this, YouTubeActivity.class);
                break;
            case R.id.btnStandAlone:
                i = new Intent(this,StandAloneActivity.class);
                break;

            default:
        }
        if (i != null) {
            startActivity(i);
        }
    }
}
